<!--
This file is part of the repository of
Thomas Lemberger's public Jupyter notebooks:
https://gitlab.com/lemberger/jupyter-introduction

SPDX-FileCopyrightText: 2020 Thomas Lemberger <https://thomaslemberger.com>

SPDX-License-Identifier: Apache-2.0
-->

# Jupyter notebook

[![Apache 2.0 License](https://img.shields.io/badge/license-Apache--2-brightgreen.svg?style=flat)](https://www.apache.org/licenses/LICENSE-2.0)
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/lemberger%2Fjupyter-introduction/master)

This repository contains multiple jupyter notebooks that give
an introduction into different parts of Jupyter.

Click the above badge to start an instance of these notebooks with mybinder.org .

## License

This project is licensed under the Apache 2.0 License with copyright by Thomas Lemberger (2020).