# This file is part of the repository of
# Thomas Lemberger's public Jupyter notebooks:
# https://gitlab.com/lemberger/jupyter-introduction
#
# SPDX-FileCopyrightText: 2020 Thomas Lemberger <https://thomaslemberger.com>
#
# SPDX-License-Identifier: Apache-2.0

matplotlib>=3.2
numpy>=1.18.4