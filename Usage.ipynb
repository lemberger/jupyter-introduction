{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction to Jupyter Notebook\n",
    "\n",
    "This notebook will give you a brief introduction to using a *Jupyter notebook*.\n",
    "A notebook consists of multiple cells. Each cell can be run individually and in arbitrary order, or you can run all cells one after the other.\n",
    "\n",
    "A cell is either text or code: If you run a text cell, the markdown contained in that cell is rendered and displayed.\n",
    "If you run a code cell, the code in that cell is executed on the current state of the notebook."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Running the notebook"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can run a selected cell by hitting `Shift + Enter` or clicking the `Play`-Button in the toolbar.\n",
    "\n",
    "Go ahead and try to execute that cell:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Good work, I got executed!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To signal that the cell was run, the notebook will add a number in bracket `[1]:` left of the cell's content.\n",
    "This number increases with each run - we will see why this is information is important in the next step.\n",
    "\n",
    "In addition, the notebook will automatically highlight the next cell. This makes it easy to rapidly execute multiple cells one after the other:\n",
    "Go ahead, execute the following cells by quickly pressing `Shift+Enter` multiple times!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"First cell ran!\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Second cell ran!\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Third cell ran!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you want to automatically execute all cells of the notebook sequentially, click on the fast-forward button in the toolbar."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The shared state (kernel)\n",
    "\n",
    "Each notebook has a single state that is shared between all cells, called the *kernel*.\n",
    "Whenever you execute a cell, it modifies that state by running functions and setting variable values.\n",
    "Usually, the cells of a notebook should be executed top-to-bottom,\n",
    "but that order has no influence on the program state: only the order of executions does!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can try that out below:\n",
    "\n",
    "1. Run *Cell B*: First select the cell that says *Cell B* at its top by clicking on it. Then press `Shift+Enter` or the play button in the toolbar.  \n",
    "This runs the cell and sets `x = 1`.\n",
    "\n",
    "2. Run *Cell A*: Select the cell that says *Cell A* at its top by clicking on it. Then press `Shift+Enter` or the play button in the toolbar.  \n",
    "This runs the cell and sets `x = 0`.\n",
    "3. Now run *Cell C* the same way you ran the other two cells: it will print that the value of x is 0.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Cell A\n",
    "# Run me after the cell below\n",
    "x = 0"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Cell B\n",
    "# Run me first!\n",
    "x = 1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Cell C\n",
    "# Run me last\n",
    "print(f\"Value of x: {x}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**What happened?** *Cell A* sets `x = 0` and *Cell B* sets `x = 1` afterwards.\n",
    "But not the order of the cells is important, but in which order\n",
    "you run them: Since you first ran *Cell B* and then *Cell A*, you first set `x = 1` **and then** `x = 0`, not the other way around.\n",
    "When you ran *Cell C* as last cell, it looked at the value of `x` in the global state and found `x = 0`.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Another experiment:** The following experiment will show you that even a single cell's behavior can change if executed multiple times.\n",
    "The cell below tries to increase a variable `run_count` by 1.\n",
    "If the variable doesn't exist, it is initialized with 0.\n",
    "\n",
    "Run this cell multiple times: Click on it, press `Shift+Enter`,\n",
    "click on it again, and so on."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "try:\n",
    "    run_count += 1\n",
    "except NameError:\n",
    "    run_count = 0\n",
    "\n",
    "print(f\"Value of run_count: {run_count}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**What happened?** The cell always works on the global state. In its first run, `run_count` does not exist, so a `NameError` is raised and the cell sets `run_count = 0`.\n",
    "In each consecutive run, `run_count` already exists in the global state,\n",
    "so the cell uses that and increases the value of `run_count` by one.\n",
    "The `print` statement will tell you the current value of `run_count` at the end of each run."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Conclusion.**\n",
    "It is very important to know two things: (1) the order of cell execution is important when you start to experiment with notebooks. (2) a single cell may be executed multiple times and will always work on the current global state.\n",
    "After each time a code cell is run, the cell gets an increasing number in brackets left to it, for example `[4]:`. This number shows you the order in which the cells were run and makes it easy to check that everything was run in the order you intended."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Resetting the kernel\n",
    "\n",
    "If you are confused about the notebook's state or something\n",
    "is not working as expected, you can reset the notebook's kernel by pressing the circling arrow in the toolbar.\n",
    "This will reset the whole notebook's state."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Displaying values"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Up to now, I used `print` statements to show information.\n",
    "But there's a simpler way: Jupyter notebooks always display the value of the last statement in a code cell:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "run_count"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## License\n",
    "\n",
    "This notebook is part of Thomas Lemberger's Jupyter notebooks: https://gitlab.com/lemberger/jupyter-introduction\n",
    "\n",
    "It is licensed under [Apache-2.0](http://apache.org/licenses/LICENSE-2.0.html),\n",
    "© [Thomas Lemberger](https://thomaslemberger.com) 2020."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
